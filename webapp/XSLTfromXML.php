<?php require 'header.php'; ?>

<div class="container">
     <div class="row">

			<div class="twelve columns">
					<?php require 'topnav.php'; ?>
					 </div>
		</div>
	<div class="row">

				<div class="twelve columns">
						<h2>Data Transfer and Web Services</h2>
					 </div>
	</div>

		<div class="row">
		    <div class="twelve columns"> 
						   
					<?php require 'nav.php'; ?>

			</div>
			</div>
		 	
<div class="row">

			<!-- Content -->
		 	<div class="twelve columns">
<br/><br/>		 	    
 
 <a href="restaurantDB.php" id="link">XML Generator, XSL applied</a><br /><br />

					

<a href="results.php" id="link">Encoded Json Data Generator</a>


<script type="text/javascript">
 

$(document).ready(function (){
    $("#btn").click(function(){       
        /* set no cache */
        $.ajaxSetup({ cache: false });
        
        $.getJSON("results.php", function(data){
            var html = [];
 
            /* loop through array */
            html.push("<table style=\"width:100%'\"><tr><th>Restaurant ID</th><th>Restaurant Name </th><th>Restaurant Description</th><th>Restaurant Location</th></tr>");
            $.each(data, function(index, d){           
                html.push("<tr> <td>", d.rest_id, "</td> ",
                          " <td>", d.name, "</td> ",
                          " <td>", d.description,  "</td> ",
                          "<td> ", d.location, 
                          
                          "</td></tr>");
            });
            html.push("</table><br>");
 
 
            $("#serverdata").html(html.join('')).css("background-color", "#669999");
        }).error(function(jqXHR, textStatus, errorThrown){ /* assign handler */
            /* alert(jqXHR.responseText) */
            alert("error occurred!");
        });
    });
});

function hideshow(){
        $("#serverdata").toggle();
}
</script>



<br/><br/>
    <button id="btn" class="button-primary">Process data from json using JQuery</button>
    <button id="btn2" class="button-primary" onclick="hideshow()">Hide/Reshow Data</button>
<div id="serverdata"></div>
<br/>
<a href="DomDocument.php" id="link">Dom Document of above data</a> 
		 	    




<br/><br/>
		 	</div></div>		 	
		 	
		 	
		 	
</div>

<?php require 'footer.php'; ?>