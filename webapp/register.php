<?php require 'header.php'; ?>

<div class="container">
     <div class="row">

			<div class="twelve columns">
					<?php require 'topnav.php'; ?>
					 </div>
		</div>
	<div class="row">

					 
				</div><div class="twelve columns">
						<h2>Registration Page</h2>
					 </div>


	<div class="row">
		    <div class="twelve columns">
						   
					<?php require 'nav.php'; ?>

			</div>
			</div>
     <div class="row">
			<!-- Content -->
		 	<div class="twelve columns">
				   	
<form method="post" action="registeruser.php" id="register">    
<div class="row">
            <div class="six columns">
		<label for="firstname">First Name: </label>
		<input class="u-full-width" placeholder="Jack" type="text" name="firstname" required /><br>
		</div>
		<div class="six columns">
		<label for="lastname">Last Name: </label>
		<input class="u-full-width" placeholder="Kenna" type="text" name="lastname" required/><br>
		</div></div>
		<div class="row">
		    <div class="twelve columns">
		<label for="email">Email: </label>
		<input class="u-full-width" placeholder="jackkenna@jackkenna.com" type="text" name="email" id="email" required/>
		</div></div>
		<div id="feedback"></div>
<div class="row">
		    <div class="six columns">
		        <br />
		<label for="password">Password: </label>
		<input class="u-full-width" type="password" placeholder="password" name="password" id="password" required />
		</div></div>
		<div id="feedbackpass"></div><br>
		 <label for="spam">What is 10 + 2?</label>
        <input class="u-full-width" type="text" placeholder="Verify you're human" name="spam" id="spam" required>
        <br><br>
        <label for="g">Human Verification reCaptcha</label>
        <div class="g-recaptcha" data-sitekey="6LfazxATAAAAAK6jNMWaJm9foxcRQyT0fEUH_iqT" id="g"></div>
<br>
		<input class="button-primary" type="submit" value="Submit" name="Register"/>
		<input class="button-primary" type="reset" value="Start over" /><br><br>
		<div id="formres2">
		<?php
		
if (isset($_SESSION['error1']))
{
	echo $_SESSION['error1'];
	unset($_SESSION['error1']);
}
?>
</div>
	</form>
	
	<script type="text/javascript" src="assets/js/jquery.js"></script>
  <script type="text/javascript">
$(document).ready(function() {
    		$('#feedback').load('check.php');

    		$('#email').keyup(function() {
    			$.post('check.php', { email: register.email.value },
    				function(result) {
$('#feedback').html(result).show();
    			});
    		});
    	
    		$('#feedbackpass').load('passcheck.php');

    		$('#password').keyup(function() {
    			$.post('passcheck.php', { password: register.password.value },
    				function(result) {
$('#feedbackpass').html(result).show();
    			});
    		});
});
    	</script>
		 	</div>
		</div>

</div>
<?php require 'footer.php'; ?>