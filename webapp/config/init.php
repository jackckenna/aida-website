<?php 
session_start();
require_once 'config.php';

function __autoload($class_name) {
  require_once 'includes/classes/'.$class_name . '.php';
}

$mysqli_conn = new mysqli($db['hostname'],$db['username'],$db['password'], $db['database']);

		if ($mysqli_conn -> connect_errno) {//check the connection
			print "Failed to connect to MySQL: (" . $mysqli_conn -> connect_errno . ") " . $mysqli_conn -> connect_error;
		}