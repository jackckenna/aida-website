<?php require 'header.php'; ?>

<div class="container">
     <div class="row">

			<div class="twelve columns">
					<?php require 'topnav.php'; ?>
					 </div>
		</div>
	<div class="row">

				<div class="twelve columns">
						<h2>City Information</h2>
					 </div>
	</div>

		<div class="row">
		    <div class="twelve columns"> 
						   
					<?php require 'nav.php'; ?>

			</div>
			</div>
		 	
<div class="row">

			<!-- Content -->
		 	
<div class="six columns">
    
    <br />
    <button id="btn" class="button-primary">Get Weather</button>
    <button id="btn2" class="button-primary" onclick="hideshow()">Hide/Reshow Data</button>
    <br/><br/>

    <a href="http://api.wunderground.com/api/85d34cd151ea37ae/geolookup/conditions/q/GB/Leeds.json" id="link6">Wunderground Json Link</a>
<br /><br />
    <br />
<div id="weather1">
    
</div>

</div>
<div class="six columns">
    <br/>
    <p>Here is the map of leeds, click on the pinpoint to zoom in to leeds mcdonalds restaurant<p>
    <p><br/>To get the weather for leeds click the button "Get Weather"<br/>The weather is provided by Weather Underground<p>
<div id="map" style="width: 450px; height: 300px"></div>
<br/><br/>

<script>
$(document).ready(function() {
  $( "#btn" ).click(function() {
$.ajax({
  url : "http://api.wunderground.com/api/85d34cd151ea37ae/geolookup/conditions/q/GB/Leeds.json",
  dataType : "jsonp",
  success : function(parsed_json) {
  var location = parsed_json['location']['city'];
  var cont = parsed_json['location']['country_name'];
  var temp_c = parsed_json['current_observation']['temp_c'];
  var title = parsed_json['current_observation']['weather'];
  var wind = parsed_json['current_observation']['wind_mph'];
  var dt = parsed_json['current_observation']['local_time_rfc822'];
   var dn = parsed_json['current_observation']['icon_url'];
   var url = parsed_json['current_observation']['image']['url'];
   
 $('#weather1').html("<img src="+url+" alt='image' id='wu' >" +"<br/>Today, " + dt + " <br/> The current temperature in " + location+" / "+ cont + " is " + temp_c + " degrees celcius. <br/>It is " + title + " Outside.<br/> The wind speed is " + wind + "MPH<br/><br/>"+title+"<br/> <img src="+dn+" alt='image' > ");



  }
  });
 });
});

function hideshow(){
        $("#weather1").toggle();
}
</script>


</div>
</div>
<div class="row">
    <div class="twelve columns">
<br />

<a href="http://api.geonames.org/citiesJSON?north=44.1&south=-9.9&east=-22.4&west=55.2&lang=de&username=jackckenna" id="link6">Geonames Json Link</a>
<br /><br />

     <script>
        $(function() {
            $( "#diobox" ).dialog({
                position: { my: "right", at: "right", of: window }
                
            });
                 });
            </script>
            
<div id="diobox" title="City Information">
  <p>Using Angular JS I have Processed an online Json file !</p>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>

<div ng-app="countrys" ng-controller="Ctrl" id="app"> 
<h3>Find a City</h3>
Search:<input ng-model="query" type="text">
<table class="app">
  <tr ng-repeat="x in names | filter:query">
    <td>{{ x.toponymName}}</td>
    <td>{{ x.countrycode }}</td>
    <td><a href="http://{{ x.wikipedia }}" id="link">{{ x.wikipedia }}</a></td>
    

  </tr>
</table>

</div>

<script>

var app = angular.module('countrys', []);
app.controller('Ctrl', function($scope, $http) {
    $http.get("http://api.geonames.org/citiesJSON?north=44.1&south=-9.9&east=-22.4&west=55.2&lang=de&username=jackckenna")
    .success(function (response) {$scope.names = response.geonames;});
});

</script>


         <script>
        
            function initMap() {
              var myLatLng = {lat: 53.796073, lng: -1.542694};
            
              var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 8,
                center: {lat: 53.796073, lng: -1.542694},
                zoomControl: true, //yes
                scaleControl: true
              });
            
              var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Click to zoom'
              });
              
              marker.addListener('click', function() {
                map.setZoom(18); //how far to zoom on click
                map.setCenter(marker.getPosition());
              });
              
                map.addListener('center_changed', function() {
                    // 3 seconds after moved form marker, pan back 
                    window.setTimeout(function() {
                      map.panTo(marker.getPosition());
                    }, 3000);
                  });
              
               
            } 
            
         </script>
         <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6JVzUMkPV-u53uGwalKBOBsQ8FIue-Xk&signed_in=true&callback=initMap"></script>


		 	</div></div>		 	
		 	
		 	
		 	
</div>

<?php require 'footer.php'; ?>