<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="/">
<html>
<body>
<h2>Restaurants</h2>
<table border="1">
<tr bgcolor="orange">
<th align="left">ID</th>
<th align="left">Name</th>
<th align="left">Description</th>
<th align="left">Location</th>
</tr>
<xsl:for-each select="restaurants/restaurant">
<tr>
<td>
<xsl:value-of select="restaurant_id"/>
</td>
<td>
<xsl:value-of select="restaurant_name"/>
</td>
<td>
<xsl:value-of select="restaurant_description"/>
</td>
<td>
<xsl:value-of select="restaurant_location"/>
</td>
</tr>
</xsl:for-each>
</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>