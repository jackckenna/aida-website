<?php require 'header.php'; ?>

<div class="container">
     <div class="row">

			<div class="twelve columns">
					<?php require 'topnav.php'; ?>
					 </div>
		</div>
	<div class="row">

				<div class="twelve columns">
						<h2>About Us</h2>
					 </div>
	</div>

		<div class="row">
		    <div class="twelve columns"> 
						   
					<?php require 'nav.php'; ?>

			</div>
			</div>
			<div class="row">

			<!-- Content -->
		 	<div class="six columns">
					
									
				
	<script>
var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var labelIndex = 0;

function initialize() {
  var macdonalds = { lat: 51.507351, lng: -0.127758 };
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    center: macdonalds
  });

  // This event listener calls addMarker() when the map is clicked.
  google.maps.event.addListener(map, 'click', function(event) {
    addMarker(event.latLng, map);
  });

  // Add a marker at the center of the map.
  addMarker(macdonalds, map);
  
 
  
}

// Adds a marker to the map.
function addMarker(location, map) {
  // Add the marker at the clicked location, and add the next-available label
  // from the array of alphabetical characters.
  var marker = new google.maps.Marker({
    position: location,
    label: labels[labelIndex++ % labels.length],
    draggable:true,
    map: map
  });
  
   var infowindow = new google.maps.InfoWindow({
  content:"<h6>Mcdonalds Head Office</h6><hr/>This is the current location <br/> of the Mcdonald's head office"
  });

google.maps.event.addListener(marker, 'click', function() {
  infowindow.open(map,marker);
  });
  
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>

<p>Displayed below is the head office of Mcdonald's<br/><br/>If you'd like to visit them, here is there pinpointed location<br/><br/><br/></p>
<div id="map" style="width: 420px; height: 420px">
    </div>
    </div>	
    <div class="six columns">
    <script>
    function initMap() {
  var map = new google.maps.Map(document.getElementById('myloc'), {
    center: {lat: -34.397, lng: 150.644},
    zoom: 13
  });
  var infoWindow = new google.maps.InfoWindow({map: myloc});

  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      infoWindow.setPosition(pos);
      infoWindow.setContent('Location found.');
      map.setCenter(pos);
    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }
  
  
  
 var showPosition = function (position) {
    var userLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    // create the marker using the users location.
    var marker = new google.maps.Marker({
        position: userLatLng,
        title: 'Your Location',
        map: map    });
        
    
 marker.addListener('click', function() {
                map.setZoom(18); //how far to zoom on click
                map.setCenter(marker.getPosition());
              });    
}

 navigator.geolocation.getCurrentPosition(showPosition); 
}
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
}

    </script>

      <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6JVzUMkPV-u53uGwalKBOBsQ8FIue-Xk&callback=initMap">
    </script>
    
    
    
    
<p>Here is your pinpointed location on the map<br/></p></br>
<p>Click on the pin point to zoom<br/></p></br>

<div id="myloc" style="width: 420px; height: 420px">
    </div>
		 	</div></div>
		</div>

</div>
<?php require 'footer.php'; ?>