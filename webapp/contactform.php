<?php require 'header.php'; ?>

<div class="container">
    <div class="row">

			<div class="twelve columns">
					<?php require 'topnav.php'; ?>
					 </div>
		</div>
	<div class="row">

			<div class="twelve columns">
						<h2>Contact Form </h2>
					 </div>
		</div>

		<div class="row">
		    <div class="twelve columns">
						   
					<?php require 'nav.php'; ?>

			</div>
			</div>

<div class="row">
			<!-- Content -->
		 	<div class="twelve columns">
		 	    
 <script>
  $(function() {
      $( "#date" ).datepicker({ dateFormat: 'dd-mm-yy' }); });
  </script> 
		 	    
		 	    
		 	    <form method="post" action="action.php" id="contactform">
                          <div class="row">
                            <div class="six columns">
                              <label for="firstname">First Name</label>
                              <input class="u-full-width" type="text" placeholder="Adam" name="firstname" id="firstname">
                            </div>
                              <div class="six columns">
                              <label for="lastname">Last Name</label>
                              <input class="u-full-width" type="text" placeholder="Smith" name="lastname" id="lastname">
                            </div>
                            </div>
                            <div class="row">   
                            <div class="six columns">
                              <label for="email">Email</label>
                              <input class="u-full-width" type="email" placeholder="test@jackkenna.com" name="email" id="email" required>
                            </div>
                            <div class="six columns">
                              <label for="date">Date you want to be contacted</label>
                              <input class="u-full-width" type="text" placeholder="Click to choose" name="date" id="date" >
                            </div>
                    </div>
                         <label for="spam">What is 10 + 2?</label>
                              <input class="u-full-width" type="text" placeholder="Verify you're human" name="spam" id="spam" required>
                              <br><br>
                                 <label for="g">Human Verification reCaptcha</label>
                               <div class="g-recaptcha" data-sitekey="6LfazxATAAAAAK6jNMWaJm9foxcRQyT0fEUH_iqT" id="g"></div>
                               <br>
                          <label for="comments">Comments</label>
                          <textarea class="u-full-width" placeholder="Enter comments here..." name="comments" id="comments" required></textarea>
                          <input class="button-primary" type="submit" value="submit">
                         
                         
                </form>
		 	    
		 	    <div id="formres">	 	    
		 	  <?php  if( isset($_SESSION['error1']))
		 	    {
		 	        echo 	$_SESSION['error1'];
		 	       unset($_SESSION['error1']);
		 	    }
		       
		 	    ?>
		 	    </div>
		 	</div>
		</div>

</div>
<?php require 'footer.php'; ?>