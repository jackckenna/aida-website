<?php 
//Create a DomDocument object

  $xml = new DOMDocument;

  // Load the XML source

  $xml->load('restaurants.xml');

//Similar with XSL

  $xsl = new DOMDocument;

  $xsl -> load('restaurants.xsl');
  // Create and Configure the transformer

 $proc = new XSLTProcessor;

  // attach the xsl rules

 $proc -> importStyleSheet($xsl);

  //Output

  echo $proc -> transformToXML($xml);


?>