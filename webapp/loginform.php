	<?php

if (!isset($_SESSION['tryusername']))
{
	$_SESSION['tryusername']=null;
}
?>
	<form method="post" action="login.php" id="loginform">
	<div class="row">
 <div class="six columns">
		<label for="username">Username(Email): </label>
	<input class="u-full-width" placeholder="john@jackkenna.com" type="text" id="username" name="username" value="<?php echo $_SESSION['tryusername']; ?>"/><br>
		</div>
	<div class="six columns">
		<label for="password">Password: </label>
		<input class="u-full-width" placeholder="password" type="password" id="password" name="password" /><br>
		</div>
		</div>
		<label class="checkbox">
                    <input type="checkbox" value="remember-me" id="remember_me"> Remember me
                </label>
                <br/>
		<input class="button-primary" type="submit" name="submit" value="Log in" />
		<input class="button-primary" type="reset" value="Start over" />
		<br>
		<div id="formres3">
		<?php if (isset($_SESSION['error']))
{
	echo $_SESSION['error'];
	unset($_SESSION['error']);
}
?>
</div>
<script>
    $(function() {
 
                if (localStorage.chkbx && localStorage.chkbx != '') {
                    $('#remember_me').attr('checked', 'checked');
                    $('#username').val(localStorage.usrname);
                    $('#password').val(localStorage.pass);
                } else {
                    $('#remember_me').removeAttr('checked');
                    $('#username').val('');
                    $('#password').val('');
                }
 
                $('#remember_me').click(function() {
 
                    if ($('#remember_me').is(':checked')) {
                        // save username and password
                        localStorage.usrname = $('#username').val();
                        localStorage.pass = $('#password').val();
                        localStorage.chkbx = $('#remember_me').val();
                    } else {
                        localStorage.usrname = '';
                        localStorage.pass = '';
                        localStorage.chkbx = '';
                    }
                });
            });

</script>