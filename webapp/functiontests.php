<?php require 'header.php'; ?>

<div class="container">
     <div class="row">

			<div class="twelve columns">
					<?php require 'topnav.php'; ?>
					 </div>
		</div>
	<div class="row">

					 
				</div><div class="twelve columns">
						<h2>Functions Test</h2>
					 </div>


		<div class="row">
		    <div class="twelve columns">
						   
					<?php require 'nav.php'; ?>

			</div>
			</div>
     <div class="row">
			<!-- Content -->
		 	<div class="twelve columns">
					
				<h3>Built-in Functions</h3>
            	  <?php 		
            	    $strvar = "restaurant";
            	    $intvar = 19;
            	    $floatvar = 27.579;
            	    $product_array = array("id"=>234, "description"=>"restaurant", "type"=>"burger");
            	    $url = "www.jackkenna.com";
            	?>
            	
                        a) Length of the string Restaurant = <?php echo strlen($strvar); ?><br />
                        b) Restaurant  to uppercase = <?php echo strtoupper($strvar); ?> <br />
                        c) Substring 'some' in www.somesite.com starts at index position = <?php echo strpos($url, "jack"); ?><br />
                        d) Restaurants encrypted with md5 = <?php echo md5($strvar); ?><br />
                        e) Get the variable type of 19 = <?php echo gettype($intvar); ?><br />
                        f) Is <?php echo $floatvar; ?> numeric? The answer will be 1(true) or 0(false)= <?php echo is_numeric($floatvar); ?><br />
                        g) Format the number 27.579 to 1 decimal place = <?php echo number_format($floatvar, 1); ?><br />
                        h) Print the product_array = <br/> <?php print_r($product_array); ?> <br />
                        i) Number of items in product_array = <?php echo count($product_array); ?><br />
                        j) Is restaurant in the array? = <?php echo in_array($strvar, $product_array); ?><br />
                        k) Add "price"=>2.45 onto the product array and display = <?php $product_array['price']=2.45; 
                        print_r($product_array);?><br/>
                        l) Explode www.somesite.com into an array (separated by ".") and display = <?php print_r(explode(".",$url)); ?> <br/>
                        m) Format todays date like this : <?php echo date('D jS F Y'); ?>
                        
                        			
					
					
					
					
		 	</div>
		</div>

</div>
<?php require 'footer.php'; ?>