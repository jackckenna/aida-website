<?php 
$xmlDomDoc = new DOMDocument();

  $xmlDomDoc->load("restaurants.xml");

  $rest = $xmlDomDoc->getElementsByTagName('restaurant');

  foreach( $rest as $rest )  { 

  $xmlIDs = $rest->getElementsByTagName( "restaurant_id" ); 

  $xmlID = $xmlIDs->item(0)->nodeValue; 
    
  $xmlNames = $rest->getElementsByTagName( "restaurant_name" ); 

  $xmlName = $xmlNames->item(0)->nodeValue; 

  $xmlDescs = $rest->getElementsByTagName( "restaurant_description" ); 

  $xmlDesc = $xmlDescs->item(0)->nodeValue; 

  $xmlLoca = $rest->getElementsByTagName( "restaurant_location" ); 

  $xmlLoc = $xmlLoca->item(0)->nodeValue;     

  echo "$xmlID - $xmlName - $xmlDesc - $xmlLoc.<br />"; 

  } 
?>