<?php require 'header.php'; ?>

<div class="container">
     <div class="row">

			<div class="twelve columns">
					<?php require 'topnav.php'; ?>
					 </div>
		</div>
	<div class="row">

				<div class="twelve columns">
						<h2>Search Through Our Restaurants</h2>
					 </div>
	</div>

		<div class="row">
		    <div class="twelve columns"> 
						   
					<?php require 'nav.php'; ?>

			</div>
			</div>
		 	
<div class="row">

			<!-- Content -->
		 	<div class="twelve columns">
<br/><br/>		 	    

<a href="encodedjson.php" id="link">Encoded Json Generator</a><br/>

<br/>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>

<div ng-app="rest" ng-controller="Ctrl" id="app"> 
<h3>Find a Restaurant using Angular JS</h3>
Search:<input ng-model="query" type="text">
<table class="app">
  <tr ng-repeat="restaurants in rest | filter:query">
    <td>{{ restaurants.rest_id}}</td>
    <td>{{ restaurants.name }}</td>
    <td>{{ restaurants.description }}</td>
    <td>{{ restaurants.location }}</td>
  </tr>
</table>

</div>

<script>

var app = angular.module('rest', []);
app.controller('Ctrl', function($scope, $http) {
    $http.get("encodedjson.php")
    .success(function (response) {$scope.rest = response.restaurant;});
});

</script>








<br/><br/>
		 	</div></div>		 	
		 	
		 	
		 	
</div>

<?php require 'footer.php'; ?>