<?php
require 'config/init.php';

$msg='';
if($_SERVER["REQUEST_METHOD"] == "POST")
{
$recaptcha=$_POST['g-recaptcha-response'];
if(!empty($recaptcha))
{
include("getCurlData.php");
$google_url="https://www.google.com/recaptcha/api/siteverify";
$secret='6LfazxATAAAAAD67Mm-3tmsAq0Wif0xQO1kBLpot';
$ip=$_SERVER['REMOTE_ADDR'];
$url=$google_url."?secret=".$secret."&response=".$recaptcha."&remoteip=".$ip;
$res=getCurlData($url);
$res= json_decode($res, true);

if($res['success'])
{



//Gather details submitted
$firstname=$_POST['firstname'];
$lastname=$_POST['lastname'];
$email=$_POST['email'];
$spam=$_POST['spam'];
$comments=$_POST['comments'];
$date=$_POST['date'];

$email = filter_var($email, FILTER_SANITIZE_EMAIL);



if($firstname==null || $lastname==null || $email==null || $spam==null){
    $_SESSION['error1']= "All fields required";
    header ('Location: '. $_SERVER['HTTP_REFERER']);
}
else if (!filter_var($email, FILTER_VALIDATE_EMAIL))
	{
	
	$_SESSION['error1']= "$email is not valid";
	header ('Location: '. $_SERVER['HTTP_REFERER']);
	}

else if($spam!=="12"){
    $_SESSION['error1']= "Human Verification Incorrect";
	header ('Location: '. $_SERVER['HTTP_REFERER']);
    
}
	
else
{

// the message
$msg = "

<html>
<head>
<title>Jack Kenna Comments</title>
</head>
<body>
Dear $firstname $lastname,
<br/><br/>Thank you, your comments have been received <br/><br/> You will be contacted within 5 days of $date <br/><br/> Kind Regards <br/>Jack Kenna 
</body>
</html>
";

// use wordwrap() if lines are longer than 70 characters
$msg = wordwrap($msg,70);
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
// send email
mail($email,"Jack Kenna Comments",$msg,$headers);


$msg2 = "

<html>
<head>
<title>Jack Kenna Comments</title>
</head>
<body>
Jack,
<br/><br/> You have received comments from $firstname $lastname ($email)<br/><br/> They said: <br/>$comments <br/><br/>They want to be contacted within 5 days of $date <br/><br/>Kind Regards <br/>Jack Kenna 
</body>
</html>
";

// use wordwrap() if lines are longer than 70 characters
$msg = wordwrap($msg,70);
$headers2 = "MIME-Version: 1.0" . "\r\n";
$headers2 .= "Content-type:text/html;charset=UTF-8" . "\r\n";
// send email
mail("jackkenna@jackkenna.com","Jack Kenna Comments",$msg2,$headers2);
    
    $_SESSION['error1']= "Your comments have been sent <br/> A conformation email has been sent";
    header ('Location: '. $_SERVER['HTTP_REFERER']);
    
}
}
else
{
 $_SESSION['error1']= "Please re-enter your reCAPTCHA";
    header ('Location: '. $_SERVER['HTTP_REFERER']);
}

}
else
{
 $_SESSION['error1']= "Please re-enter your reCAPTCHA";
    header ('Location: '. $_SERVER['HTTP_REFERER']);
}

}


?>
