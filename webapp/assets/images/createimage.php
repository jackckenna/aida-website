<?php 
header("Content-type: image/jpeg");


//Some Test Text

  $text = "Here I have created an Image";


//Set the image width and height

  $width = 500;

  $height = 400;


//Create the image resource

  $image = ImageCreate($width, $height);


//We are making three colors, white, black and gray

  $white = ImageColorAllocate($image, 255, 255, 255);

  $black = ImageColorAllocate($image, 0, 0, 0);

  $orange = ImageColorAllocate($image, 220, 210, 60);


//Make the background black

  ImageFill($image, 0, 0, $black);


//Add randomly generated string in white to the image

  ImageString($image, 3, 30, 3, $text, $white);


//Throw in some lines to make it a little bit harder for any bots to break

  ImageRectangle($image, 0, 0, $width - 1, $height - 1, $orange);

  imageline($image, 0, $height / 2, $width, $height / 2, $orange);

  //Output the newly created image in jpeg format

  ImageJpeg($image);

  //Free up resources

  ImageDestroy($image);
?>