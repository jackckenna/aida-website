<?php 
//header ("Content-Type:text/xml");//Tell browser to expect xml
include ("config/init.php");

$result=$mysqli_conn->query("SELECT * FROM restaurant");

//Top of xml file
$_xml = '<?xml version="1.0"?>'; 
$_xml = '<?xml-stylesheet type="text/xsl" href="restaurants.xsl"?>'; 
$_xml .="<restaurants>"; 
while($row = $result->fetch_assoc()) { 
$_xml .="<restaurant>"; 
$_xml .="<restaurant_id>".$row['rest_id']."</restaurant_id>"; 
$_xml .="<restaurant_name>".$row['name']."</restaurant_name>"; 
$_xml .="<restaurant_description>".$row['description']."</restaurant_description>"; 
$_xml .="<restaurant_location>".$row['location']."</restaurant_location>"; 
$_xml .="</restaurant>"; 
} 
$_xml .="</restaurants>"; 
//Parse and create an xml object using the string
$xmlobj=new SimpleXMLElement($_xml);
//And output
//print $xmlobj->asXML();
//or we could write to a file
//$xmlobj->asXML("restaurants.xml");

  $xml = new DOMDocument;

  // Load the XML source

  $xml=$xmlobj;

//Similar with XSL

  $xsl = new DOMDocument;

  $xsl -> load('restaurants.xsl');
  // Create and Configure the transformer

 $proc = new XSLTProcessor;

  // attach the xsl rules

 $proc -> importStyleSheet($xsl);

  //Output

  echo $proc -> transformToXML($xml);



?>