<nav>
	<ul>
		<li><a href="index.php">Index</a></li>
		<li><a href="#">Tests</a>
			<ul>
				<li><a href="initialtests.php">Initial Tests</a></li>
				<li><a href="functiontests.php">Function Tests</a></li>
				<li><a href="ootests.php">OO Tests</a></li>
				<li><a href="oodev.php">OO Development</a></li>
			</ul>
		</li>
		<li><a href="#">Media Pages</a>
			<ul>
				<li><a href="video_audio.php">Video/Audio</a></li>
				<li><a href="images.php">Images</a></li>
			</ul>
		</li>
		<li><a href="#">Inspiration</a></li>
	</ul>
</nav>