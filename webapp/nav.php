<nav>
<label for="show-menu" class="show-menu">Show Menu</label>
<input type="checkbox" id="show-menu" value="button" class="hidden">
	<ul id="menu">
		<li><a href="index.php">Index</a></li>
		<li><a href="#">Tests</a>
			<ul class="hidden">
				<li><a href="initialtests.php">Initial Tests</a></li>
				<li><a href="functiontests.php">Function Tests</a></li>
				<li><a href="ootests.php">OO Tests</a></li>
				<li><a href="oodev.php">OO Development</a></li>
			</ul>
		</li>
		<li><a href="#">Media Pages</a>
			<ul class="hidden">
				<li><a href="video_audio.php">Video/Audio</a></li>
				<li><a href="images.php">Images</a></li>
			</ul>
		</li>
		<li><a href="XSLTfromXML.php">Data Transfer and Web Services</a>
		<ul class="hidden">
				<li><a href="XSLTfromXML.php">General</a></li>
				<li><a href="cityinfo.php">City Information</a></li>
			</ul>
			</li>
			<li><a href="restaurantSearch.php">Restaurant </a>
		<ul class="hidden">
				<li><a href="restaurantSearch.php">Search Restaurant</a></li>
			</ul>
			</li>
		<li><a href="about.php">About Us</a></li>
	</ul>
</nav>